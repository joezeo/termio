package com.toocol.ssh.utilities.utils;

/**
 * @author ZhaoZhe (joezane.cn@gmail.com)
 * @date 2022/4/12 11:10
 */
public class FilePathUtil {

    public static final String CURRENT_FOLDER_PREFIX = "./";

    public static final String ROOT_FOLDER_PREFIX = "/";

    public static final String PARENT_FOLDER_PREFIX = "../";

    public static final String USER_FOLDER = "~";

}
