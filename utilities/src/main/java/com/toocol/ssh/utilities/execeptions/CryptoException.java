package com.toocol.ssh.utilities.execeptions;

/**
 * @author ：JoeZane (joezane.cn@gmail.com)
 * @date: 2022/5/2 21:42
 * @version: 0.0.1
 */
public class CryptoException extends RuntimeException{

    public CryptoException(String message) {
        super(message);
    }

}
