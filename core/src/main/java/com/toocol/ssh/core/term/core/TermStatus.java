package com.toocol.ssh.core.term.core;

/**
 * @author ：JoeZane (joezane.cn@gmail.com)
 * @date: 2022/4/23 2:15
 * @version: 0.0.1
 */
public enum TermStatus {
    TERMIO(),
    SHELL()
    ;

    TermStatus() {
    }
}
