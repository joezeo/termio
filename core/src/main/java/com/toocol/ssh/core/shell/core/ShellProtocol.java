package com.toocol.ssh.core.shell.core;

/**
 * @author ：JoeZane (joezane.cn@gmail.com)
 * @date: 2022/4/30 20:29
 * @version: 0.0.1
 */
public enum ShellProtocol {
    SSH,
    MOSH
}
