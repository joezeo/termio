package com.toocol.ssh.core.term.core;

import com.toocol.ssh.utilities.action.AbstractCharAction;

/**
 * @author ZhaoZhe (joezane.cn@gmail.com)
 * @date 2022/4/25 18:13
 */
public abstract class TermCharAction extends AbstractCharAction<Term> {

}
